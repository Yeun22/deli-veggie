<?php

namespace App\DataFixtures;

use App\Entity\BasketVegetable;
use Faker\Factory;
use App\Entity\Product;
use App\Entity\UserAmap;
use App\Entity\UserClient;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use FakerRestaurant\Provider\en_US\Restaurant;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $userAmap = new UserAmap;
        $userAmap->setName("Bonamapétit")
            ->setEmail('bonamap@mail.com')
            ->setPassword($this->passwordHasher->hashPassword($userAmap, 'password'))
            ->setAdress('25 rue des tournesols')
            ->setPostalCode('78000')
            ->setCity('Versailles')
            ->setBio(true)
            ->setPhoneNumber('01 05 28 79 63')
            ->setInformations('Ouvert du lundi au samedi de 8h à 12h. tout est bio !')
            ->setWebSite('https://bonamappetit.fr/');
        $manager->persist($userAmap);


        $faker = Factory::create();

        $faker->addProvider(new Restaurant($faker));


        //More amap
        for ($i = 0; $i < 6; $i++) {
            $userAmap = new UserAmap;
            $userAmap->setName($faker->company())
                ->setEmail($faker->companyEmail())
                ->setPassword($this->passwordHasher->hashPassword($userAmap, 'password'))
                ->setAdress($faker->address())
                ->setPostalCode($faker->numberBetween(11000, 95000))
                ->setCity($faker->city())
                ->setBio($faker->boolean())
                ->setPhoneNumber($faker->phoneNumber())
                ->setInformations($faker->realText())
                ->setWebSite("monsiteweb.com");


            for ($j = 0; $j < 4; $j++) {
                $basket = new BasketVegetable();
                $basket->setName("Basket " . $faker->colorName())
                    ->setDescription($faker->realText())
                    ->setPrice($faker->numberBetween(10, 70))
                    ->setUserAmap($userAmap);

                for ($v = 0; $v < 3; $v++) {
                    $vegetable = new Product;
                    $vegetable->setName($faker->vegetableName());
                    $vegetable->setPicture("https://picsum.photos/200");
                    $manager->persist($vegetable);

                    $basket->addProduct($vegetable);
                    $userAmap->addProduct($vegetable);
                }
                $manager->persist($basket);
            }
            $manager->persist($userAmap);
        }


        $userClient = new UserClient();
        $userClient->setName("John Doe")
            ->setEmail("johndoe@mail.com")
            ->setPassword($this->passwordHasher->hashPassword($userClient, 'password'))
            ->setAdress("25 rue du test")
            ->setPostalCode('95000')
            ->setCity('Paris')
            ->setPhoneNumber('06 00 00 00 00');
        $manager->persist($userClient);

        // // creation de produit legume
        // for ($i = 0; $i < 15; $i++) {
        //     $vegetable = new Product;
        //     $vegetable->setName($faker->vegetableName());
        //     $vegetable->setPicture("https://picsum.photos/200");
        //     $manager->persist($vegetable);
        // }

        // // creation de produit fruit
        // for ($i = 0; $i < 15; $i++) {
        //     $fruit = new Product;
        //     $fruit->setName($faker->fruitName());
        //     $fruit->setPicture("https://picsum.photos/200");
        //     $manager->persist($fruit);
        // }

        $manager->flush();
    }
}
