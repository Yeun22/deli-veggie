<?php

namespace App\Repository;

use App\Entity\BasketVegetable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BasketVegetable|null find($id, $lockMode = null, $lockVersion = null)
 * @method BasketVegetable|null findOneBy(array $criteria, array $orderBy = null)
 * @method BasketVegetable[]    findAll()
 * @method BasketVegetable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasketVegetableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BasketVegetable::class);
    }

    // /**
    //  * @return BasketVegetable[] Returns an array of BasketVegetable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BasketVegetable
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
