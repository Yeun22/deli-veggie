<?php

namespace App\Form;

use App\Entity\UserAmap;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmapProfilFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('bio')
            ->add('adress')
            ->add('postalCode')
            ->add('city')
            ->add('phoneNumber')
            ->add('informations')
            ->add('website')
            ->add('email');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserAmap::class,
        ]);
    }
}
