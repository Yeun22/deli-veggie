<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClientController extends AbstractController
{

    /**
     * @Route("/client/dashboard", name="client_dashboard")
     */
    public function dashboardClient(): Response
    {
        return $this->render('client/dashboard.html.twig', []);
    }
}
