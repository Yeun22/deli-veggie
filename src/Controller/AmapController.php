<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProductRepository;
use App\Entity\BasketVegetable;
use App\Form\ProductFormType;
use App\Form\BasketFormType;
use App\Form\AmapProfilFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Security;

class AmapController extends AbstractController
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/amap/dashboard", name="dashboard_amap")
     */
    public function dashboardAmap(): Response
    {
        return $this->render('amap/dashboard.html.twig', []);
    }

    /**
     * @Route("/amap/dashboard/create/product", name="create_product")
     */
    public function createProduct(Request $request, EntityManagerInterface $entityManager, Security $security): Response
    {
        $user = $security->getUser();
        $form = $this->createForm(ProductFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $form->get('products')->getData();
            foreach ($products as $product ) {
                $user->addProduct($product);
            }
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Product Added in your shop!');

            return new RedirectResponse("/amap/dashboard");
        }

        return $this->render('amap/createProduct.html.twig', [
            'productForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/amap/dashboard/create/basket", name="create_basket")
     */
    public function createBasket(Request $request, EntityManagerInterface $entityManager, Security $security): Response
    {
        $user = $security->getUser();
        $basket = new BasketVegetable();
        $form = $this->createForm(BasketFormType::class, $basket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $basket = $form->getData();

            $basket->setUserAmap($user);
            $entityManager->persist($basket);
            $entityManager->flush();
            $this->addFlash('success', 'Basket Added in your shop!');

            return new RedirectResponse("/amap/dashboard");
        }

        return $this->render('amap/createBasket.html.twig', [
            'basketForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/amap/dashboard/edit", name="edit_amap")
     */
    public function editAmap(Request $request, EntityManagerInterface $entityManager, Security $security): Response
    {
        $user = $security->getUser();
        $form = $this->createForm(AmapProfilFormType::class, $user);
        $form->handleRequest($request);
        $form->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            $this->addFlash('success', 'Your profile is updated!');
            return new RedirectResponse("/amap/dashboard");
        }

        return $this->render('amap/editProfil.html.twig', [
            'amapProfilForm' => $form->createView(),
        ]);
    }
}