<?php

namespace App\Controller;

use App\Repository\UserAmapRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function homepage(): Response
    {
        return $this->render('home/homepage.html.twig', []);
    }

    /**
     * @Route("/research/amaps", name="user_research_amap")
     */
    public function listingAmap(Request $request, UserAmapRepository $amaprepo): Response
    {

        if ($request->getMethod() === "POST") {
            $research = $request->request->get('localisation-input');

            $amaps = $amaprepo->findBy(['city' => $research]);
            if ($amaps === []) {
                $amaps = $amaprepo->findBy(['postalCode' => $research]);
                if ($amaps === []) {
                    $this->addFlash("error", "We don't find your research, try something else");
                    $amaps = $amaprepo->findAll();
                }
            }
        } else {
            $this->addFlash("error", "We don't find your research, try something else");
            $amaps = $amaprepo->findAll();
        }

        return $this->render('home/listAmaps.html.twig', ["amaps" => $amaps]);
    }
    /**
     * @Route("/amaps/{id}/details", name="amap_details")
     */
    public function detailsAmap($id, UserAmapRepository $amaprepo): Response
    {

        $amap = $amaprepo->findOneBy(["id" => $id]);

        return $this->render('home/detailsAmap.html.twig', ["amap" => $amap]);
    }
}
