<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @ORM\ManyToMany(targetEntity=BasketVegetable::class, mappedBy="products")
     */
    private $basketsVegetable;

    /**
     * @ORM\ManyToMany(targetEntity=UserAmap::class, mappedBy="products")
     */
    private $amaps;

    public function __construct()
    {
        $this->basketsVegetable = new ArrayCollection();
        $this->amaps = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return Collection|BasketVegetable[]
     */
    public function getBasketsVegetable(): Collection
    {
        return $this->basketsVegetable;
    }

    public function addBasketsVegetable(BasketVegetable $basketsVegetable): self
    {
        if (!$this->basketsVegetable->contains($basketsVegetable)) {
            $this->basketsVegetable[] = $basketsVegetable;
            $basketsVegetable->addProduct($this);
        }

        return $this;
    }

    public function removeBasketsVegetable(BasketVegetable $basketsVegetable): self
    {
        if ($this->basketsVegetable->removeElement($basketsVegetable)) {
            $basketsVegetable->removeProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserAmap[]
     */
    public function getAmaps(): Collection
    {
        return $this->amaps;
    }

    public function addAmap(UserAmap $amap): self
    {
        if (!$this->amaps->contains($amap)) {
            $this->amaps[] = $amap;
            $amap->addProduct($this);
        }

        return $this;
    }

    public function removeAmap(UserAmap $amap): self
    {
        if ($this->amaps->removeElement($amap)) {
            $amap->removeProduct($this);
        }

        return $this;
    }
}
