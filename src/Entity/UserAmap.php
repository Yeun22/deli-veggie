<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserAmapRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=UserAmapRepository::class)
 * @ORM\Table(name="user_amap")
 */
class UserAmap extends User
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="text")
     */
    private $informations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $webSite;

    /**
     * @ORM\Column(type="boolean")
     */
    private $bio;

    /**
     * @ORM\OneToMany(targetEntity=BasketVegetable::class, mappedBy="userAmap", orphanRemoval=true)
     */
    private $basketsVegetable;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="amaps")
     */
    private $products;

    public function __construct()
    {
        $this->basketsVegetable = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getInformations(): ?string
    {
        return $this->informations;
    }

    public function setInformations(string $informations): self
    {
        $this->informations = $informations;

        return $this;
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function setWebSite(?string $webSite): self
    {
        $this->webSite = $webSite;

        return $this;
    }

    public function getBio(): ?bool
    {
        return $this->bio;
    }

    public function setBio(bool $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * @return Collection|BasketVegetable[]
     */
    public function getBasketsVegetable(): Collection
    {
        return $this->basketsVegetable;
    }

    public function addBasketsVegetable(BasketVegetable $basketsVegetable): self
    {
        if (!$this->basketsVegetable->contains($basketsVegetable)) {
            $this->basketsVegetable[] = $basketsVegetable;
            $basketsVegetable->setUserAmap($this);
        }

        return $this;
    }

    public function removeBasketsVegetable(BasketVegetable $basketsVegetable): self
    {
        if ($this->basketsVegetable->removeElement($basketsVegetable)) {
            // set the owning side to null (unless already changed)
            if ($basketsVegetable->getUserAmap() === $this) {
                $basketsVegetable->setUserAmap(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }
}
