<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112160850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE basket_vegetable (id INT AUTO_INCREMENT NOT NULL, user_amap_id INT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_5D2FE92CAA778E2 (user_amap_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE basket_vegetable_product (basket_vegetable_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_A99ACD94737B0F15 (basket_vegetable_id), INDEX IDX_A99ACD944584665A (product_id), PRIMARY KEY(basket_vegetable_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, picture VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, discr VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_amap (id INT NOT NULL, name VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, postal_code VARCHAR(30) NOT NULL, city VARCHAR(255) NOT NULL, phone_number VARCHAR(50) NOT NULL, informations LONGTEXT NOT NULL, web_site VARCHAR(255) DEFAULT NULL, bio TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_amap_product (user_amap_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_DF08D717AA778E2 (user_amap_id), INDEX IDX_DF08D7174584665A (product_id), PRIMARY KEY(user_amap_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_client (id INT NOT NULL, name VARCHAR(255) NOT NULL, adress VARCHAR(255) NOT NULL, postal_code VARCHAR(30) NOT NULL, city VARCHAR(255) NOT NULL, phone_number VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE basket_vegetable ADD CONSTRAINT FK_5D2FE92CAA778E2 FOREIGN KEY (user_amap_id) REFERENCES user_amap (id)');
        $this->addSql('ALTER TABLE basket_vegetable_product ADD CONSTRAINT FK_A99ACD94737B0F15 FOREIGN KEY (basket_vegetable_id) REFERENCES basket_vegetable (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE basket_vegetable_product ADD CONSTRAINT FK_A99ACD944584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_amap ADD CONSTRAINT FK_B4B3701ABF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_amap_product ADD CONSTRAINT FK_DF08D717AA778E2 FOREIGN KEY (user_amap_id) REFERENCES user_amap (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_amap_product ADD CONSTRAINT FK_DF08D7174584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_client ADD CONSTRAINT FK_A2161F68BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE basket_vegetable_product DROP FOREIGN KEY FK_A99ACD94737B0F15');
        $this->addSql('ALTER TABLE basket_vegetable_product DROP FOREIGN KEY FK_A99ACD944584665A');
        $this->addSql('ALTER TABLE user_amap_product DROP FOREIGN KEY FK_DF08D7174584665A');
        $this->addSql('ALTER TABLE user_amap DROP FOREIGN KEY FK_B4B3701ABF396750');
        $this->addSql('ALTER TABLE user_client DROP FOREIGN KEY FK_A2161F68BF396750');
        $this->addSql('ALTER TABLE basket_vegetable DROP FOREIGN KEY FK_5D2FE92CAA778E2');
        $this->addSql('ALTER TABLE user_amap_product DROP FOREIGN KEY FK_DF08D717AA778E2');
        $this->addSql('DROP TABLE basket_vegetable');
        $this->addSql('DROP TABLE basket_vegetable_product');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_amap');
        $this->addSql('DROP TABLE user_amap_product');
        $this->addSql('DROP TABLE user_client');
    }
}
