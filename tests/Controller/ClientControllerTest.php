<?php

namespace App\Tests\Controller;

use App\Repository\UserClientRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();

        $userClientRepository = static::getContainer()->get(UserClientRepository::class);
        $testUser = $userClientRepository->findOneBy(['email' => 'johndoe@mail.com']);
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/client/dashboard');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("header");
        $this->assertSelectorExists("footer");
        $this->assertSelectorTextContains('h1.display-4', 'Hi John Doe');

        $pAdress = $crawler->filter('p.mb-0');

        $dataFind = $pAdress->each(function ($node, $i) {
            return $node->text();
        });

        $dataWaiting = ["25 rue du test", "95000 Paris", "06 00 00 00 00"];

        for ($i = 0; $i < count($dataFind); $i++) {
            $this->assertTrue($dataFind[$i] === $dataWaiting[$i]);
        }

        $this->assertSelectorTextContains('a.btn-info', 'Modify informations');
        $this->assertSelectorTextContains('h3', 'Last orders');

        $dataLinks = $crawler->filter('a');
        $dataLink = $dataLinks->each(function ($node, $i) {
            if ($i === 2) {
                return $node->attr('href');
            }
        });
        $this->assertTrue($dataLink[2] === '/research/amaps');
    }
}
