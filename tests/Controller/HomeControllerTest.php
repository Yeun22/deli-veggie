<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();

        $this->assertSelectorExists("header");

        $this->assertSelectorTextContains('h1', 'deli-veggie');
        $this->assertPageTitleContains("Déli-Veggie");

        $this->assertCount(6, $crawler->filter('h3.bg-success'));

        $link = $crawler->selectLink('Sign In')->link();
        $client->click($link);
        $this->assertResponseIsSuccessful();


        $this->assertSelectorExists("footer");
    }
}
