<?php

namespace App\Tests\Controller;

use App\Repository\UserAmapRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AmapControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $userAmapRepository = static::getContainer()->get(UserAmapRepository::class);
        $testUser = $userAmapRepository->findOneByEmail('bonamap@mail.com');
        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/amap/dashboard');

        $adressAmap = $crawler->filter('p.mb-0');
        $btnLink = $crawler->filter('a.btn');
        $liProduct = $crawler->filter('li');
        $basketNamePrice = $crawler->filter('div#basketNamePrice');
        $basketDesc = $crawler->filter('p#basketDescription');
        $basketProd = $crawler->filter('footer#basketProductName');

        $dataFind = $adressAmap->each(function ($node, $i) {
            return $node->text();
        });
        $dataFindLink = $btnLink->each(function ($node, $i) {
            return $node->attr('href');
        });
        $dataFindTextLink = $btnLink->each(function ($node, $i) {
            return $node->text('href');
        });
        $dataFindTextProduct = $liProduct->each(function ($node, $i) {
            return $node->text();
        });
        $dataFindNamePriceBasket = $basketNamePrice->each(function ($node, $i) {
            return $node->text();
        });
        $dataFindDescBasket = $basketDesc->each(function ($node, $i) {
            return $node->text();
        });
        $dataFindProductInBasket = $basketProd->each(function ($node, $i) {
            return $node->text();
        });
        $dataWaitingAdress = ["25 rue des tournesols", "78000 Versailles", "01 05 28 79 55"];
        $dataWaitingLink = ["/amap/dashboard/edit", "/amap/dashboard/create/product", "/amap/dashboard/create/basket"];
        $dataWaitingTextLink = ["Modify informations", "Add product", "Add veggies/fruits basket"];
        $dataWaitingTextProduct = [
            "Carrot",
            "Tomato",
            "Cucumber",
            "Potato",
            "Cherry",
            "Peach",
            "Raspberry",
            "Grape",
            "Lemon",
            "Apple",
            "Strawberry",
            "Blueberry",
            "Banana",
            "Blackberry",
            "Mango"
        ];
        $dataWaitingNamePriceBasket = ["Legumes - 20€", "Fruits - 20€"];
        $dataWaitingDescBasket = ["Carottes, pommes de terres et tomates bio!", "Un panier garnie de Pêches, Pommes, et cerises"];
        $dataWaitingProductsInBasket = ["Carrot Tomato Potato", "Cherry Peach Apple"];

        for ($i = 0; $i < count($dataFind); $i++) {
            $this->assertTrue($dataFind[$i] === $dataWaitingAdress[$i]);
        }
        for ($i = 0; $i < count($dataFindLink); $i++) {
            $this->assertTrue($dataFindLink[$i] === $dataWaitingLink[$i]);
        }
        for ($i = 0; $i < count($dataFindTextLink); $i++) {
            $this->assertTrue($dataFindTextLink[$i] === $dataWaitingTextLink[$i]);
        }
        for ($i = 0; $i < count($dataFindTextProduct); $i++) {
            $this->assertTrue($dataFindTextProduct[$i] === $dataWaitingTextProduct[$i]);
            $this->assertCount(count($dataFindTextProduct) , $dataWaitingTextProduct);
        }
        for ($i = 0; $i < count($dataFindNamePriceBasket); $i++) {
            $this->assertTrue($dataFindNamePriceBasket[$i] === $dataWaitingNamePriceBasket[$i]);
        }
        for ($i = 0; $i < count($dataFindDescBasket); $i++) {
            $this->assertTrue($dataFindDescBasket[$i] === $dataWaitingDescBasket[$i]);
        }
        for ($i = 0; $i < count($dataFindProductInBasket); $i++) {
            $this->assertTrue($dataFindProductInBasket[$i] === $dataWaitingProductsInBasket[$i]);
            $this->assertCount(count($dataFindProductInBasket) , $dataWaitingProductsInBasket);
        }

        $this->assertResponseIsSuccessful();

        $this->assertTrue($client->getResponse()->isSuccessful());

        $this->assertSelectorTextContains('h1#welcomeAmap', 'Hi Bonamapétit');
        $this->assertSelectorTextContains('p#informationsAmap', 'Ouvert du lundi au samedi de 8h à 12h. tout est bio !');
    }
}
